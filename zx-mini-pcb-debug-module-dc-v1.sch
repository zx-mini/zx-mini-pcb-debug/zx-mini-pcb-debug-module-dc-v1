EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "zx-mini-pcb-debug-module-dc-v1"
Date ""
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:L L1
U 1 1 5FC70851
P 4450 2200
F 0 "L1" V 4650 2200 50  0000 C CNN
F 1 "LQM21PN1R0MC0D" V 4550 2250 50  0000 C CNN
F 2 "Inductor_SMD:L_1008_2520Metric_Pad1.43x2.20mm_HandSolder" H 4450 2200 50  0001 C CNN
F 3 "" H 4450 2200 50  0001 C CNN
	1    4450 2200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2150 2750 2150 2850
Wire Wire Line
	2150 2250 2150 2150
Wire Wire Line
	2750 2750 2750 2850
Wire Wire Line
	2750 2250 2750 2150
$Comp
L lib:TPS61240DRV D2
U 1 1 60293B57
P 5300 2300
F 0 "D2" H 5300 2667 50  0000 C CNN
F 1 "TPS61240DRV" H 5300 2576 50  0000 C CNN
F 2 "Package_SON:WSON-6-1EP_2x2mm_P0.65mm_EP1x1.6mm_ThermalVias" H 4500 1850 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/tps61240.pdf?ts=1613204880662&ref_url=https%253A%252F%252Fwww.google.com%252F" H 4500 1850 50  0001 C CNN
	1    5300 2300
	1    0    0    -1  
$EndComp
$Comp
L lib:SiP32431DR3 D4
U 1 1 60299C50
P 8000 2300
F 0 "D4" H 8000 2667 50  0000 C CNN
F 1 "SiP32431DR3" H 8000 2576 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 8000 2300 50  0001 C CNN
F 3 "" H 8000 2300 50  0001 C CNN
	1    8000 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 602EEF76
P 4000 2450
F 0 "C1" H 4050 2550 50  0000 L CNN
F 1 "4.7uF" H 4050 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4038 2300 50  0001 C CNN
F 3 "~" H 4000 2450 50  0001 C CNN
	1    4000 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 602F7A62
P 4300 2450
F 0 "C2" H 4350 2550 50  0000 L CNN
F 1 "1uF" H 4350 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4338 2300 50  0001 C CNN
F 3 "~" H 4300 2450 50  0001 C CNN
	1    4300 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 603095AA
P 6100 2450
F 0 "C3" H 6150 2550 50  0000 L CNN
F 1 "4.7uF" H 6150 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6138 2300 50  0001 C CNN
F 3 "~" H 6100 2450 50  0001 C CNN
	1    6100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2400 5750 2400
Wire Wire Line
	5750 2400 5750 2200
Wire Wire Line
	5750 2200 5700 2200
Wire Wire Line
	5750 2200 6100 2200
Connection ~ 5750 2200
Wire Wire Line
	4600 2200 4900 2200
Wire Wire Line
	4300 2300 4900 2300
Connection ~ 4300 2300
Wire Wire Line
	4000 2300 4300 2300
$Comp
L power:VCC #PWR04
U 1 1 6033BAD9
P 3700 2200
F 0 "#PWR04" H 3700 2050 50  0001 C CNN
F 1 "VCC" H 3715 2373 50  0000 C CNN
F 2 "" H 3700 2200 50  0001 C CNN
F 3 "" H 3700 2200 50  0001 C CNN
	1    3700 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 60346047
P 4000 2700
F 0 "#PWR06" H 4000 2450 50  0001 C CNN
F 1 "GND" H 4005 2527 50  0000 C CNN
F 2 "" H 4000 2700 50  0001 C CNN
F 3 "" H 4000 2700 50  0001 C CNN
	1    4000 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 60346F1E
P 4300 2700
F 0 "#PWR07" H 4300 2450 50  0001 C CNN
F 1 "GND" H 4305 2527 50  0000 C CNN
F 2 "" H 4300 2700 50  0001 C CNN
F 3 "" H 4300 2700 50  0001 C CNN
	1    4300 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 6034F8D9
P 6100 2700
F 0 "#PWR011" H 6100 2450 50  0001 C CNN
F 1 "GND" H 6105 2527 50  0000 C CNN
F 2 "" H 6100 2700 50  0001 C CNN
F 3 "" H 6100 2700 50  0001 C CNN
	1    6100 2700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR010
U 1 1 60360DD2
P 6100 2200
F 0 "#PWR010" H 6100 2050 50  0001 C CNN
F 1 "+5V" H 6115 2373 50  0000 C CNN
F 2 "" H 6100 2200 50  0001 C CNN
F 3 "" H 6100 2200 50  0001 C CNN
	1    6100 2200
	1    0    0    -1  
$EndComp
Connection ~ 6100 2200
$Comp
L power:GND #PWR09
U 1 1 60362287
P 5250 2700
F 0 "#PWR09" H 5250 2450 50  0001 C CNN
F 1 "GND" H 5255 2527 50  0000 C CNN
F 2 "" H 5250 2700 50  0001 C CNN
F 3 "" H 5250 2700 50  0001 C CNN
	1    5250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2600 5350 2600
$Comp
L Device:C C4
U 1 1 603758CB
P 7050 2350
F 0 "C4" H 7100 2450 50  0000 L CNN
F 1 "1uF" H 7100 2250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7088 2200 50  0001 C CNN
F 3 "~" H 7050 2350 50  0001 C CNN
	1    7050 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 6037DE7E
P 8600 2350
F 0 "C7" H 8650 2450 50  0000 L CNN
F 1 "0.1uF" H 8650 2250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8638 2200 50  0001 C CNN
F 3 "~" H 8600 2350 50  0001 C CNN
	1    8600 2350
	1    0    0    -1  
$EndComp
Text Label 7250 2300 0    50   ~ 0
CH1_ON
$Comp
L Device:R R4
U 1 1 6039A1C9
P 7600 2450
F 0 "R4" H 7530 2404 50  0000 R CNN
F 1 "4,7k" H 7530 2495 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7530 2450 50  0001 C CNN
F 3 "~" H 7600 2450 50  0001 C CNN
	1    7600 2450
	1    0    0    1   
$EndComp
Connection ~ 7600 2300
Wire Wire Line
	7600 2300 7700 2300
Wire Wire Line
	7250 2300 7600 2300
$Comp
L power:GND #PWR016
U 1 1 603B530E
P 7600 2600
F 0 "#PWR016" H 7600 2350 50  0001 C CNN
F 1 "GND" H 7605 2427 50  0000 C CNN
F 2 "" H 7600 2600 50  0001 C CNN
F 3 "" H 7600 2600 50  0001 C CNN
	1    7600 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 603BDEC0
P 8000 2600
F 0 "#PWR019" H 8000 2350 50  0001 C CNN
F 1 "GND" H 8005 2427 50  0000 C CNN
F 2 "" H 8000 2600 50  0001 C CNN
F 3 "" H 8000 2600 50  0001 C CNN
	1    8000 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2600 8000 2600
Connection ~ 8000 2600
Wire Wire Line
	4900 2400 4800 2400
Text Label 4500 2400 0    50   ~ 0
+5_ON
$Comp
L Device:R R2
U 1 1 603DA2E5
P 4800 2550
F 0 "R2" H 4730 2504 50  0000 R CNN
F 1 "4,7k" H 4730 2595 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 2550 50  0001 C CNN
F 3 "~" H 4800 2550 50  0001 C CNN
	1    4800 2550
	1    0    0    1   
$EndComp
Connection ~ 4800 2400
Wire Wire Line
	4800 2400 4500 2400
$Comp
L power:GND #PWR08
U 1 1 603EC4B8
P 4800 2700
F 0 "#PWR08" H 4800 2450 50  0001 C CNN
F 1 "GND" H 4805 2527 50  0000 C CNN
F 2 "" H 4800 2700 50  0001 C CNN
F 3 "" H 4800 2700 50  0001 C CNN
	1    4800 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2700 5250 2600
Connection ~ 5250 2600
Wire Wire Line
	4300 2700 4300 2600
Wire Wire Line
	4000 2700 4000 2600
Wire Wire Line
	7700 2200 7050 2200
Connection ~ 7050 2200
Wire Wire Line
	7050 2200 6850 2200
Wire Wire Line
	6100 2300 6100 2200
Wire Wire Line
	6100 2700 6100 2600
$Comp
L power:GND #PWR013
U 1 1 6045FCCC
P 7050 2600
F 0 "#PWR013" H 7050 2350 50  0001 C CNN
F 1 "GND" H 7055 2427 50  0000 C CNN
F 2 "" H 7050 2600 50  0001 C CNN
F 3 "" H 7050 2600 50  0001 C CNN
	1    7050 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 2600 7050 2500
Wire Wire Line
	8300 2200 8600 2200
Connection ~ 8600 2200
$Comp
L power:GND #PWR022
U 1 1 60486747
P 8600 2600
F 0 "#PWR022" H 8600 2350 50  0001 C CNN
F 1 "GND" H 8605 2427 50  0000 C CNN
F 2 "" H 8600 2600 50  0001 C CNN
F 3 "" H 8600 2600 50  0001 C CNN
	1    8600 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 2600 8600 2500
Text GLabel 9400 2200 2    50   Input ~ 0
+5V_CH1
$Comp
L Device:R R3
U 1 1 604A69B8
P 6600 2350
F 0 "R3" V 6500 2400 50  0000 R CNN
F 1 "470" V 6700 2400 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6530 2350 50  0001 C CNN
F 3 "~" H 6600 2350 50  0001 C CNN
	1    6600 2350
	1    0    0    1   
$EndComp
$Comp
L Device:LED D3
U 1 1 604A69BE
P 6600 2650
F 0 "D3" H 6600 2500 50  0000 C CNN
F 1 "TO-1608BC-MRE" H 6593 2486 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 6600 2650 50  0001 C CNN
F 3 "https://www.oasistek.com/pdf/TO-1608BC-MRE_9201T098.pdf" H 6600 2650 50  0001 C CNN
	1    6600 2650
	0    1    -1   0   
$EndComp
Wire Wire Line
	6600 2200 6100 2200
$Comp
L power:GND #PWR012
U 1 1 604C8791
P 6600 2800
F 0 "#PWR012" H 6600 2550 50  0001 C CNN
F 1 "GND" H 6605 2627 50  0000 C CNN
F 2 "" H 6600 2800 50  0001 C CNN
F 3 "" H 6600 2800 50  0001 C CNN
	1    6600 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 604D3FC2
P 9050 2350
F 0 "R7" V 8950 2400 50  0000 R CNN
F 1 "470" V 9150 2400 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8980 2350 50  0001 C CNN
F 3 "~" H 9050 2350 50  0001 C CNN
	1    9050 2350
	1    0    0    1   
$EndComp
$Comp
L Device:LED D7
U 1 1 604D3FC8
P 9050 2650
F 0 "D7" H 9050 2500 50  0000 C CNN
F 1 "TO-1608BC-MRE" H 9043 2486 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 9050 2650 50  0001 C CNN
F 3 "https://www.oasistek.com/pdf/TO-1608BC-MRE_9201T098.pdf" H 9050 2650 50  0001 C CNN
	1    9050 2650
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 604D3FCE
P 9050 2800
F 0 "#PWR025" H 9050 2550 50  0001 C CNN
F 1 "GND" H 9055 2627 50  0000 C CNN
F 2 "" H 9050 2800 50  0001 C CNN
F 3 "" H 9050 2800 50  0001 C CNN
	1    9050 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 2200 9050 2200
Connection ~ 9050 2200
Wire Wire Line
	9050 2200 9400 2200
$Comp
L lib:SiP32431DR3 D5
U 1 1 604EC73B
P 8000 3550
F 0 "D5" H 8000 3917 50  0000 C CNN
F 1 "SiP32431DR3" H 8000 3826 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 8000 3550 50  0001 C CNN
F 3 "" H 8000 3550 50  0001 C CNN
	1    8000 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 604EC741
P 7050 3600
F 0 "C5" H 7100 3700 50  0000 L CNN
F 1 "1uF" H 7100 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7088 3450 50  0001 C CNN
F 3 "~" H 7050 3600 50  0001 C CNN
	1    7050 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 604EC747
P 8600 3600
F 0 "C8" H 8650 3700 50  0000 L CNN
F 1 "0.1uF" H 8650 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8638 3450 50  0001 C CNN
F 3 "~" H 8600 3600 50  0001 C CNN
	1    8600 3600
	1    0    0    -1  
$EndComp
Text Label 7250 3550 0    50   ~ 0
CH2_ON
$Comp
L Device:R R5
U 1 1 604EC74E
P 7600 3700
F 0 "R5" H 7530 3654 50  0000 R CNN
F 1 "4,7k" H 7530 3745 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7530 3700 50  0001 C CNN
F 3 "~" H 7600 3700 50  0001 C CNN
	1    7600 3700
	1    0    0    1   
$EndComp
Connection ~ 7600 3550
Wire Wire Line
	7600 3550 7700 3550
Wire Wire Line
	7250 3550 7600 3550
$Comp
L power:GND #PWR017
U 1 1 604EC757
P 7600 3850
F 0 "#PWR017" H 7600 3600 50  0001 C CNN
F 1 "GND" H 7605 3677 50  0000 C CNN
F 2 "" H 7600 3850 50  0001 C CNN
F 3 "" H 7600 3850 50  0001 C CNN
	1    7600 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 604EC75D
P 8000 3850
F 0 "#PWR020" H 8000 3600 50  0001 C CNN
F 1 "GND" H 8005 3677 50  0000 C CNN
F 2 "" H 8000 3850 50  0001 C CNN
F 3 "" H 8000 3850 50  0001 C CNN
	1    8000 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3850 8000 3850
Connection ~ 8000 3850
Wire Wire Line
	7700 3450 7050 3450
Connection ~ 7050 3450
$Comp
L power:GND #PWR014
U 1 1 604EC768
P 7050 3850
F 0 "#PWR014" H 7050 3600 50  0001 C CNN
F 1 "GND" H 7055 3677 50  0000 C CNN
F 2 "" H 7050 3850 50  0001 C CNN
F 3 "" H 7050 3850 50  0001 C CNN
	1    7050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3850 7050 3750
Wire Wire Line
	8300 3450 8600 3450
Connection ~ 8600 3450
$Comp
L power:GND #PWR023
U 1 1 604EC771
P 8600 3850
F 0 "#PWR023" H 8600 3600 50  0001 C CNN
F 1 "GND" H 8605 3677 50  0000 C CNN
F 2 "" H 8600 3850 50  0001 C CNN
F 3 "" H 8600 3850 50  0001 C CNN
	1    8600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3850 8600 3750
Text GLabel 9400 3450 2    50   Input ~ 0
+5V_CH2
$Comp
L Device:R R8
U 1 1 604EC779
P 9050 3600
F 0 "R8" V 8950 3650 50  0000 R CNN
F 1 "470" V 9150 3650 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8980 3600 50  0001 C CNN
F 3 "~" H 9050 3600 50  0001 C CNN
	1    9050 3600
	1    0    0    1   
$EndComp
$Comp
L Device:LED D8
U 1 1 604EC77F
P 9050 3900
F 0 "D8" H 9050 3750 50  0000 C CNN
F 1 "TO-1608BC-MRE" H 9043 3736 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 9050 3900 50  0001 C CNN
F 3 "https://www.oasistek.com/pdf/TO-1608BC-MRE_9201T098.pdf" H 9050 3900 50  0001 C CNN
	1    9050 3900
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 604EC785
P 9050 4050
F 0 "#PWR026" H 9050 3800 50  0001 C CNN
F 1 "GND" H 9055 3877 50  0000 C CNN
F 2 "" H 9050 4050 50  0001 C CNN
F 3 "" H 9050 4050 50  0001 C CNN
	1    9050 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3450 9050 3450
Connection ~ 9050 3450
Wire Wire Line
	9050 3450 9400 3450
Connection ~ 6600 2200
Wire Wire Line
	6850 3450 6850 2200
Wire Wire Line
	6850 3450 7050 3450
Connection ~ 6850 2200
Wire Wire Line
	6850 2200 6600 2200
$Comp
L lib:SiP32431DR3 D6
U 1 1 6054A211
P 8000 4800
F 0 "D6" H 8000 5167 50  0000 C CNN
F 1 "SiP32431DR3" H 8000 5076 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 8000 4800 50  0001 C CNN
F 3 "" H 8000 4800 50  0001 C CNN
	1    8000 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 6054A217
P 7050 4850
F 0 "C6" H 7100 4950 50  0000 L CNN
F 1 "1uF" H 7100 4750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7088 4700 50  0001 C CNN
F 3 "~" H 7050 4850 50  0001 C CNN
	1    7050 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 6054A21D
P 8600 4850
F 0 "C9" H 8650 4950 50  0000 L CNN
F 1 "0.1uF" H 8650 4750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8638 4700 50  0001 C CNN
F 3 "~" H 8600 4850 50  0001 C CNN
	1    8600 4850
	1    0    0    -1  
$EndComp
Text Label 7250 4800 0    50   ~ 0
CH3_ON
$Comp
L Device:R R6
U 1 1 6054A224
P 7600 4950
F 0 "R6" H 7530 4904 50  0000 R CNN
F 1 "4,7k" H 7530 4995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7530 4950 50  0001 C CNN
F 3 "~" H 7600 4950 50  0001 C CNN
	1    7600 4950
	1    0    0    1   
$EndComp
Connection ~ 7600 4800
Wire Wire Line
	7600 4800 7700 4800
Wire Wire Line
	7250 4800 7600 4800
$Comp
L power:GND #PWR018
U 1 1 6054A22D
P 7600 5100
F 0 "#PWR018" H 7600 4850 50  0001 C CNN
F 1 "GND" H 7605 4927 50  0000 C CNN
F 2 "" H 7600 5100 50  0001 C CNN
F 3 "" H 7600 5100 50  0001 C CNN
	1    7600 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 6054A233
P 8000 5100
F 0 "#PWR021" H 8000 4850 50  0001 C CNN
F 1 "GND" H 8005 4927 50  0000 C CNN
F 2 "" H 8000 5100 50  0001 C CNN
F 3 "" H 8000 5100 50  0001 C CNN
	1    8000 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 5100 8000 5100
Connection ~ 8000 5100
Wire Wire Line
	7700 4700 7050 4700
Connection ~ 7050 4700
Wire Wire Line
	7050 4700 6850 4700
$Comp
L power:GND #PWR015
U 1 1 6054A23E
P 7050 5100
F 0 "#PWR015" H 7050 4850 50  0001 C CNN
F 1 "GND" H 7055 4927 50  0000 C CNN
F 2 "" H 7050 5100 50  0001 C CNN
F 3 "" H 7050 5100 50  0001 C CNN
	1    7050 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 5100 7050 5000
Wire Wire Line
	8300 4700 8600 4700
Connection ~ 8600 4700
$Comp
L power:GND #PWR024
U 1 1 6054A247
P 8600 5100
F 0 "#PWR024" H 8600 4850 50  0001 C CNN
F 1 "GND" H 8605 4927 50  0000 C CNN
F 2 "" H 8600 5100 50  0001 C CNN
F 3 "" H 8600 5100 50  0001 C CNN
	1    8600 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 5100 8600 5000
Text GLabel 9400 4700 2    50   Input ~ 0
+5V_CH3
$Comp
L Device:R R9
U 1 1 6054A24F
P 9050 4850
F 0 "R9" V 8950 4900 50  0000 R CNN
F 1 "470" V 9150 4900 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8980 4850 50  0001 C CNN
F 3 "~" H 9050 4850 50  0001 C CNN
	1    9050 4850
	1    0    0    1   
$EndComp
$Comp
L Device:LED D9
U 1 1 6054A255
P 9050 5150
F 0 "D9" H 9050 5000 50  0000 C CNN
F 1 "TO-1608BC-MRE" H 9043 4986 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 9050 5150 50  0001 C CNN
F 3 "https://www.oasistek.com/pdf/TO-1608BC-MRE_9201T098.pdf" H 9050 5150 50  0001 C CNN
	1    9050 5150
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR027
U 1 1 6054A25B
P 9050 5300
F 0 "#PWR027" H 9050 5050 50  0001 C CNN
F 1 "GND" H 9055 5127 50  0000 C CNN
F 2 "" H 9050 5300 50  0001 C CNN
F 3 "" H 9050 5300 50  0001 C CNN
	1    9050 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 4700 9050 4700
Connection ~ 9050 4700
Wire Wire Line
	9050 4700 9400 4700
Wire Wire Line
	6850 3450 6850 4700
Connection ~ 6850 3450
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 5FCD4A0D
P 1950 2450
F 0 "J1" H 1950 2850 50  0000 C CNN
F 1 "PBS-8" H 1950 1950 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 1950 2450 50  0001 C CNN
F 3 "~" H 1950 2450 50  0001 C CNN
	1    1950 2450
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 5FE66CC7
P 2950 2450
F 0 "J3" H 2950 2850 50  0000 C CNN
F 1 "PLS-8" H 2950 1950 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 2950 2450 50  0001 C CNN
F 3 "~" H 2950 2450 50  0001 C CNN
	1    2950 2450
	1    0    0    -1  
$EndComp
Connection ~ 2150 2850
Connection ~ 2750 2850
Wire Wire Line
	2150 2850 2450 2850
$Comp
L power:GND #PWR02
U 1 1 605C8B00
P 2450 2850
F 0 "#PWR02" H 2450 2600 50  0001 C CNN
F 1 "GND" H 2455 2677 50  0000 C CNN
F 2 "" H 2450 2850 50  0001 C CNN
F 3 "" H 2450 2850 50  0001 C CNN
	1    2450 2850
	1    0    0    -1  
$EndComp
Connection ~ 2450 2850
Wire Wire Line
	2450 2850 2750 2850
Connection ~ 2150 2150
Connection ~ 2750 2150
Wire Wire Line
	2150 2150 2450 2150
$Comp
L power:VCC #PWR01
U 1 1 605E865E
P 2450 2150
F 0 "#PWR01" H 2450 2000 50  0001 C CNN
F 1 "VCC" H 2465 2323 50  0000 C CNN
F 2 "" H 2450 2150 50  0001 C CNN
F 3 "" H 2450 2150 50  0001 C CNN
	1    2450 2150
	1    0    0    -1  
$EndComp
Connection ~ 2450 2150
Wire Wire Line
	2450 2150 2750 2150
Wire Wire Line
	2150 2350 2750 2350
Wire Wire Line
	2150 2450 2750 2450
Wire Wire Line
	2150 2550 2750 2550
Wire Wire Line
	2150 2650 2750 2650
Text Label 2300 2350 0    50   ~ 0
+5_ON
Text Label 2300 2450 0    50   ~ 0
CH1_ON
Text Label 2300 2550 0    50   ~ 0
CH2_ON
Text Label 2300 2650 0    50   ~ 0
CH3_ON
Wire Wire Line
	2150 4950 2150 5050
Wire Wire Line
	2750 4950 2750 5050
$Comp
L Connector_Generic:Conn_01x08 J4
U 1 1 60647BAB
P 2950 4650
F 0 "J4" H 2950 5050 50  0000 C CNN
F 1 "PLS-8" H 2950 4150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 2950 4650 50  0001 C CNN
F 3 "~" H 2950 4650 50  0001 C CNN
	1    2950 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 60647BB4
P 2450 4300
F 0 "#PWR03" H 2450 4050 50  0001 C CNN
F 1 "GND" H 2455 4127 50  0000 C CNN
F 2 "" H 2450 4300 50  0001 C CNN
F 3 "" H 2450 4300 50  0001 C CNN
	1    2450 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	2750 4450 2750 4350
Connection ~ 2750 4350
Wire Wire Line
	2150 4650 2150 4550
Wire Wire Line
	2150 4550 2250 4550
Wire Wire Line
	2750 4650 2750 4550
Connection ~ 2750 4550
Connection ~ 2150 4550
Wire Wire Line
	2150 4850 2150 4750
Wire Wire Line
	2150 4750 2250 4750
Wire Wire Line
	2750 4850 2750 4750
Connection ~ 2750 4750
Connection ~ 2150 4750
Text GLabel 2300 4650 2    50   Input ~ 0
+5V_CH2
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 60647BA5
P 1950 4650
F 0 "J2" H 1950 5050 50  0000 C CNN
F 1 "PBS-8" H 1950 4150 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 1950 4650 50  0001 C CNN
F 3 "~" H 1950 4650 50  0001 C CNN
	1    1950 4650
	-1   0    0    -1  
$EndComp
Text GLabel 2300 4450 2    50   Input ~ 0
+5V_CH1
Connection ~ 2150 4350
Wire Wire Line
	2150 4450 2150 4350
Text GLabel 2300 4850 2    50   Input ~ 0
+5V_CH3
Wire Wire Line
	2300 4450 2250 4450
Wire Wire Line
	2250 4450 2250 4550
Connection ~ 2250 4550
Wire Wire Line
	2250 4550 2750 4550
Wire Wire Line
	2300 4650 2250 4650
Wire Wire Line
	2250 4650 2250 4750
Connection ~ 2250 4750
Wire Wire Line
	2250 4750 2750 4750
$Comp
L Device:R R1
U 1 1 60834F8C
P 3700 2350
F 0 "R1" V 3600 2400 50  0000 R CNN
F 1 "470" V 3800 2400 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3630 2350 50  0001 C CNN
F 3 "~" H 3700 2350 50  0001 C CNN
	1    3700 2350
	1    0    0    1   
$EndComp
$Comp
L Device:LED D1
U 1 1 60834F92
P 3700 2650
F 0 "D1" H 3700 2550 50  0000 C CNN
F 1 "TO-1608BC-MRE" H 3693 2486 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3700 2650 50  0001 C CNN
F 3 "https://www.oasistek.com/pdf/TO-1608BC-MRE_9201T098.pdf" H 3700 2650 50  0001 C CNN
	1    3700 2650
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 60834F98
P 3700 2800
F 0 "#PWR05" H 3700 2550 50  0001 C CNN
F 1 "GND" H 3705 2627 50  0000 C CNN
F 2 "" H 3700 2800 50  0001 C CNN
F 3 "" H 3700 2800 50  0001 C CNN
	1    3700 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 2200 4000 2200
Wire Wire Line
	4000 2200 4000 2300
Connection ~ 4000 2200
Wire Wire Line
	4000 2200 4300 2200
Connection ~ 4000 2300
Connection ~ 3700 2200
Wire Wire Line
	2150 4950 2250 4950
Connection ~ 2150 4950
Connection ~ 2750 4950
Wire Wire Line
	2300 4850 2250 4850
Wire Wire Line
	2250 4850 2250 4950
Connection ~ 2250 4950
Wire Wire Line
	2250 4950 2750 4950
Wire Wire Line
	2150 4350 2450 4350
Wire Wire Line
	2450 4300 2450 4350
Connection ~ 2450 4350
Wire Wire Line
	2450 4350 2750 4350
$EndSCHEMATC
