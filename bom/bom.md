
|       | References	  | Value	        | Footprint	                  | Quantity |
|-------|---------------|---------------|-----------------------------|----------|
| 1			| C2, C4, C5, C6	| 1uF	| C_0805_2012Metric	| 4
| 2			| C7, C8, C9	| 0.1uF	| C_0805_2012Metric	| 3
| 3			| C1, C3	| 4.7uF	| C_0805_2012Metric	| 2
| 4			| R1, R3, R7, R8, R9	| 470	| R_0805_2012Metric	| 5
| 5			| R2, R4, R5, R6	| 4,7k	| R_0805_2012Metric	| 4
| 6			| L1	| LQM21PN1R0MC0D	| L_1008_2520Metric_Pad1.43x2.20mm_HandSolder	| 1
| 7			| D1, D3, D7, D8, D9	| TO-1608BC-MRE	| LED_0603_1608Metric	| 5
| 8			| D4, D5, D6	| SiP32431DR3	| SOT-363_SC-70-6	| 3
| 9			| D2	| TPS61240DRV	| WSON-6-1EP_2x2mm_P0.65mm_EP1x1.6mm_ThermalVias	| 1
| 10		| J3, J4	| PLS-8	| PinHeader_1x08_P2.54mm_Vertical	| 2
| 11		| J1, J2	| PBS-8	| PinSocket_1x08_P2.54mm_Vertical	| 2
